#!/usr/bin/env bash
cd $(dirname $0)/..
sudo apt-get install -y git docker docker-compose
sudo docker stop
sudo dockerd &
sudo docker-compose up --build --force-recreate -d service
