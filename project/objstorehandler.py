import os
import boto3


class ObjStoreHandler:
    _ENDPOINT_URL = os.getenv('ENDPOINT_URL')
    _BUCKET_NAME = 'myinstabucket'

    def __init__(self):
        self._session = boto3.session.Session()
        self._s3 = self._session.client(
            service_name='s3',
            endpoint_url=self._ENDPOINT_URL,
            aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
            aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'),
            region_name='ru-central1'
        )

    def create_bucket(self):
        self._s3.create_bucket(Bucket=self._BUCKET_NAME)

    def put_object(self, fname, content):
        self._s3.put_object(Bucket=self._BUCKET_NAME, Key=fname, Body=content)
        link = self.make_link(fname)
        return link

    def get_object(self, fname):
        return self._s3.get_object(Bucket=self._BUCKET_NAME, Key=fname)

    @classmethod
    def make_link(cls, fname):
        return os.path.join(cls._ENDPOINT_URL, cls._BUCKET_NAME, fname)

    def list_objects(self):
        objects = self._s3.list_objects(
            Bucket=self._BUCKET_NAME).get('Contents', [])
        for key in objects:
            yield key['Key']
